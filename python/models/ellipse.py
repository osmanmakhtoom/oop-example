from .shape import Shape


class Ellipse(Shape):
    def __init__(self, semi_major_axis: int, semi_minor_axis: int):
        super().__init__(semi_major_axis, semi_minor_axis)

    def area(self) -> float():
        return self.PI * self._width * self._height

    def perimeter(self) -> float():
        return self.PI * 2 * self._width * self._width + self._height * self._height / 2 // 2

    def perimeter_with_ramanujan(self) -> float():
        pass

    def perimeter_with_cantrell(self) -> float():
        pass

    def __str__(self) -> str:
        return f"Ellipse Perimeter: {self.perimeter()} Area: {self.area()}"
