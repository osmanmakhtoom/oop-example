class NotIntException(Exception):
    def __init__(self, width=0, height=0, message="The entered value is not a numeric value."):
        self.width= width
        self.height = height
        self.message = message
        super().__init__(self.message)
        
    def __str__(self):
        return f"Width: {self.width}, Height: {self.height} -> {self.message}"