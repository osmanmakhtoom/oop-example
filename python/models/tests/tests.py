from unittest import TestCase

from ..square import Square
from ..rectangle import Rectangle
from ..circle import Circle
from ..ellipse import Ellipse

from ..exceptions import NotIntException


class OOPTest(TestCase):
    def setUp(self):
        self.square = Square(20)
        self.rectangle = Rectangle(10, 20)
        self.circle = Circle(20)
        self.ellipse = Ellipse(10, 20)

    # Test Square

    def test_square_correct_return(self):
        self.assertEqual(self.square.area(), 400)
        self.assertEqual(self.square.perimeter(), 80)

    def test_square_check_int_type(self):
        with self.assertRaises(NotIntException):
            square = Square("u")

    # Test Rectangle

    def test_rectangle_correct_return(self):
        self.assertEqual(self.rectangle.area(), 200)
        self.assertEqual(self.rectangle.perimeter(), 60)

    def test_rectangle_check_int_type(self):
        with self.assertRaises(NotIntException):
            rectangle = Rectangle("u", "g")

    # Test Circle

    def test_circle_correct_return(self):
        self.assertEqual(self.circle.area(), 1256.6370614359173)
        self.assertEqual(self.circle.perimeter(), 125.66370614359172)

    def test_circle_check_int_type(self):
        with self.assertRaises(NotIntException):
            circle = Circle("u")

    # Test Ellipse

    def test_ellipse_correct_return(self):
        self.assertEqual(self.ellipse.area(), 628.3185307179587)
        self.assertEqual(self.ellipse.perimeter(), 728.3185307179587)

    def test_ellipse_check_int_type(self):
        with self.assertRaises(NotIntException):
            ellipse = Ellipse("u", "g")
