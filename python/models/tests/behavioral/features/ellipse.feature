Feature: Ellipse shape

    For learning OOP software development methodology
    We has created the following classes:
    Shape Abstract Base class
    Ellipse Class That extends Shape class

    Scenario: Calculate area
        Given Ellipse shape major and minor for calculating the area
        When Call to Ellipse class's area method
        Then Return the area of the ellipse shape by multiplying PI with major with minor

    Scenario: Calculate perimeter
        Given Ellipse shape major and minor for calculating the perimeter
        When Call to Ellipse class's perimeter method
        Then Return the perimeter of the ellipse shape by multiplying PI with 2 with major with major plus multiplying PI with 2 with minor with minor