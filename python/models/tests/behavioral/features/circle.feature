Feature: Circle shape

    For learning OOP software development methodology
    We has created the following classes:
    Shape Abstract Base class
    Circle Class That extends Shape class

    Scenario: Calculate area
        Given Circle shape radius for calculating the area
        When Call to Circle class's area method
        Then Return the area of the circle shape by multiplying PI with radius with radius

    Scenario: Calculate perimeter
        Given Circle shape radius for calculating the perimeter
        When Call to Circle class's perimeter method
        Then Return the perimeter of the circle shape by multiplying PI with radius with 2