Feature: Rectangle shape

    For learning OOP software development methodology
    We has created the following classes:
    Shape Abstract Base class
    Rectangle Class That extends Shape class

    Scenario: Calculate area
        Given Rectangle shape width and height for calculating the area
        When Call to Rectangle class's area method
        Then Return the area of the rectangle shape by multiplying width with height

    Scenario: Calculate perimeter
        Given Rectangle shape width and height for calculating the perimeter
        When Call to Rectangle class's perimeter method
        Then Return the perimeter of the rectangle shape by multiplying 2 with width plus multiplying 2 with height