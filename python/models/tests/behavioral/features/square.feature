Feature: Square shape

    For learning OOP software development methodology
    We has created the following classes:
    Shape Abstract Base class
    Rectangle Class That extends Shape class
    Square Class That extends Rectangle class

    Scenario: Calculate area
        Given Square shape one side length for calculating the area
        When Call to Square class's area method
        Then Return the area of the square shape by multiplying one side length with itself

    Scenario: Calculate perimeter
        Given Square shape one side length for calculating the perimeter
        When Call to Square class's perimeter method
        Then Return the perimeter of the square shape by multiplying one side length with 4