from behave import *


# Area method steps


@given("Rectangle shape width and height for calculating the area")
def step_impl(context):
    pass


@when("Call to Rectangle class's area method")
def step_impl(context):
    assert True is not False


@then("Return the area of the rectangle shape by multiplying width with height")
def step_impl(context):
    assert context.failed is False


# Perimeter method steps


@given("Rectangle shape width and height for calculating the perimeter")
def step_impl(context):
    pass


@when("Call to Rectangle class's perimeter method")
def step_impl(context):
    assert True is not False


@then(
    "Return the perimeter of the rectangle shape by multiplying 2 with width plus multiplying 2 with height"
)
def step_impl(context):
    assert context.failed is False
