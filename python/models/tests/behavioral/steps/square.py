from behave import *


# Area method steps


@given("Square shape one side length for calculating the area")
def step_impl(context):
    pass


@when("Call to Square class's area method")
def step_impl(context):
    assert True is not False


@then("Return the area of the square shape by multiplying one side length with itself")
def step_impl(context):
    assert context.failed is False


# Perimeter method steps


@given("Square shape one side length for calculating the perimeter")
def step_impl(context):
    pass


@when("Call to Square class's perimeter method")
def step_impl(context):
    assert True is not False


@then("Return the perimeter of the square shape by multiplying one side length with 4")
def step_impl(context):
    assert context.failed is False
