from behave import *


# Area method steps


@given("Circle shape radius for calculating the area")
def step_impl(context):
    pass


@when("Call to Circle class's area method")
def step_impl(context):
    assert True is not False


@then("Return the area of the circle shape by multiplying PI with radius with radius")
def step_impl(context):
    assert context.failed is False


# Perimeter method steps


@given("Circle shape radius for calculating the perimeter")
def step_impl(context):
    pass


@when("Call to Circle class's perimeter method")
def step_impl(context):
    assert True is not False


@then("Return the perimeter of the circle shape by multiplying PI with radius with 2")
def step_impl(context):
    assert context.failed is False
