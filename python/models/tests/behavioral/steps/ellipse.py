from behave import *


# Area method steps


@given("Ellipse shape major and minor for calculating the area")
def step_impl(context):
    pass


@when("Call to Ellipse class's area method")
def step_impl(context):
    assert True is not False


@then("Return the area of the ellipse shape by multiplying PI with major with minor")
def step_impl(context):
    assert context.failed is False


# Perimeter method steps


@given("Ellipse shape major and minor for calculating the perimeter")
def step_impl(context):
    pass


@when("Call to Ellipse class's perimeter method")
def step_impl(context):
    assert True is not False


@then(
    "Return the perimeter of the ellipse shape by multiplying PI with 2 with major with major plus multiplying PI with 2 with minor with minor"
)
def step_impl(context):
    assert context.failed is False
