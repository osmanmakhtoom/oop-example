from .rectangle import Rectangle


class Square(Rectangle):
    def __init__(self, length_of_side: int):
        super().__init__(length_of_side, length_of_side)

    def __str__(self) -> str:
        return f"Square Perimeter: {self.perimeter()} Area: {self.area()}"
