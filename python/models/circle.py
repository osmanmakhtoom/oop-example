from .shape import Shape
from math import pi


class Circle(Shape):
    def __init__(self, radius: int):
        super().__init__(radius, 0)

    def area(self) -> float:
        return self.PI * self._width * self._width

    def perimeter(self) -> float:
        return self.PI * (self._width * 2)

    def __str__(self) -> str:
        return f"Circle Perimeter: {self.perimeter()} Area: {self.area()}"
