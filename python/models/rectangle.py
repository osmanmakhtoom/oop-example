from .shape import Shape


class Rectangle(Shape):
    def __init__(self, width: int, height: int):
        super().__init__(width, height)

    def area(self) -> int:
        return self._width * self._height

    def perimeter(self) -> int:
        return 2 * self._width + 2 * self._height

    def __str__(self) -> str:
        return f"Rectangle Perimeter: {self.perimeter()} Area: {self.area()}"
