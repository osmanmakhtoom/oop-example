from abc import ABC, abstractclassmethod
import math
from .exceptions import NotIntException


class Shape(ABC):

    PI = math.pi
    _width: int
    _height: int

    def __init__(self, width: int, height: int):
        if not isinstance(width, int) or not isinstance(height, int):
            raise NotIntException(width, height)

        self._width = width
        self._height = height

    @abstractclassmethod
    def area(self):
        pass

    @abstractclassmethod
    def perimeter(self):
        pass
