from models.square import Square
from models.rectangle import Rectangle
from models.circle import Circle
from models.ellipse import Ellipse


square = Square(20)
print(square)

rectangle = Rectangle(100, 150)
print(rectangle)

circle = Circle(20)
print(circle)

ellipse = Ellipse(20, 30)
print(ellipse)
